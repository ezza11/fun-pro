-- LAZY EVALUATION EXERCISE

-- make divisor function that takes an int, and return list of number that divides n
divisor n = [x | x <- [1..n], n `mod` x == 0]

-- make quicksort implementation
-- HOW TO DO QUICKSORT ??
-- find pivot then partition the list into two subArray
quick [] = []
quick (x:xs) = leftside ++ [x] ++ rightside
                where leftside = quick [left | left <- xs, left <= x]
                      rightside = quick [right | right <- xs, right > x]
-- encounter some error !!
-- make sure all recursive have a base case



-- make infinite list of permutation
-- TO BE CONTINUED

-- make infite list of prime number that uses Sieve of Erastothenes
--HOW??

-- make infinite list of triplePythagoras
triple = [(x,y,z) | z <- [5..], y <- [1 .. z-1], x <- [1 .. y-1], x*x + y * y == z * z]
