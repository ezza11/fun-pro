-- Generate KPK function
kpk x y 
    | x `mod` y == 0 = x
    | y `mod` x == 0 = y
    | otherwise  = x * y

-- Generate max list using foldl
max_ xs = foldl (\acc x -> if acc > x then acc else x) 0 xs

-- flip
-- flip :: (a -> b -> c) -> ()
flips f x y z = f y x z