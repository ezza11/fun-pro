-- Defining  length using map and sum
changeToOne x = 1
length_ xs = sum(map changeToOne xs)

-- Define iter so that iter n f x = f(f(...(fx)))
iter 0 f x = x
iter n f x = f (iter(n-1) f x)

-- define sum of natural number 1 - n using map and foldr
-- using non foldr
sumNaturalNum xs = sum(map (\x -> x * x) xs)
-- using foldr
sumNatural xs = foldr (\x acc -> acc + x * x) 0 xs

-- Define flip function
flipp f x y = f y x


-- LIST COMPREHESION
-- rewrite the following list comprehension using map and filter

-- [x+1 | x <- xs]
addOne xs = map (+1) xs

-- [x+y | x <- xs , y <- ys]
addXY xs ys = concat (map (\x -> map (\y -> x + y) ys)xs)

-- [x+2 | x <- xs, x > 3]
addTwo xs = map (+2) (filter (>3) xs)

-- [x+3 | (x,_) <- xys]
addThree xs = map (\x -> fst x + 3) xs

-- [x+4 | (x,y) <- xys, x+y < 5]
addFour xs = map (\x -> fst x + 4) (filter (\x -> fst x + snd x > 5) xs)

-- [X+5 | Just x <- xs]
-- HOWWW ???
-- lets see whats happening
five mxs = [ x+5 | Just x <-mxs ]
-- SO we need Just a in the list
addJust (Just a) = a + 5
addFive xs = map addJust xs

-- Rewrite function to list comprehension
-- map (+3) xs
mappu xs = [x+3 | x<-xs]

-- filter (>7) xs
filterru xs = [x | x <- xs, x > 7]

-- concat (map (\x -> map (\y -> (x,y)) ys) xs)
concattu xs ys = [(x,y) | x <- xs, y <- ys]

-- filter (>3) (map (\(x,y) -> x+y) xys)
filtr xs = [x+y | (x,y) <- xs, x+y > 3]