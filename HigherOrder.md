# Higher Order Notes
### This is the part when i need to explain something

#### What doesmap (+1) (map (+1) xs)do? Can you conclude anything in general about properties ofmap f (map g xs), wherefandgare arbitrary functions?
Map adalah sebuah fungsi dari haskell yang akan membuat sebuah list baru. Fungsi map mempunyai dua parameter, parameter pertama adalah fungsi yang akan diaplikasikan kepada elemen pada list, sedangkan parameter kedua adalah list awal. Cara kerja dari map adalah mengambil elemen head pada list yang terdapat pada parameter kedua lalu mengaplikasikannya dengan fungsi yang ada pada parameter pertama. 
Contoh:
```haskell
map (+2) [1,2,3]
```
Snippet kode diatas pertama tama akan mengambil 1 dari list dan menambahkan dua, lalu mengambil dua dari list dan menambahkan 2, terakhir akan mengambil tiga dari list dan menambahkannya dengan 2.
Maka untuk snippet pada soal dimana
```haskell
map f (map g xs)
```
Pertama-tama map f (...) akan dievaluasi, namun karena terdapat fungsi map dalam kurung, maka map g xs juga akan dievaluasi yg nantinya akan mengembalikan sebuah list baru yang telah mengaplikasikan fungsi g kepada seluruh elemen dari xs, sehingga akan menjadi map f (someList). map f lalu melanjutkan evaluasinya dengan mengaplikasikan fungsi f kepada someList(hasil return dari map g xs).


#### What is the type and effect of the following function? (\n -> iter n succ)
Fungsi diatas tidak bisa dijalankan, karena fungsi iter masih kurang satu parameter, namun jika fungsi diatas diubah menjadi (\n x -> iter n succ x), ini akan menjalankan fungsi succ x sebanyak n kali.

# How does the function mystery xs = foldr (++) (map sing xs) where sing x = [x] behaves?
Pertama coba jalanin dulu fungsinya.
map sing xs, akan mengambil satu persatu elemen pada list xs dan membungkusnya kedalam sebuah list, ex. [1,2,3] -> [[1], [2], [3]]. lalu foldr akan mengambil list yang sudah diproses dengan map dari kanan ke kiri. Setiap elemen yang terambil akan di concat dengan accumulator (dalam hal ini []). So, [3] ++ [] yang akan berubah menjadi [3], lalu [2] ++ [3] yang akan menjadi [2,3], finally [1] ++ [2,3] yang akan menjadi [1,2,3]. Pada dasarnya fungsi tersebut tidak akan mengembalikan list yang sama.