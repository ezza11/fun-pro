## Lazy evaluation explanation

#### Uraikan langkah evaluasi dari ekspresi berikut: ​[x + y | x <- [1..4], y <- [2..4], x> y]

Ekpresi berikut akan mengembalikan sebuah list x + y dimana x > y. nilai dari x dan y didapat dari definisi pada sebelah kanan simbol <-. X adalah list yang berisi [1,2,3,4] sedangkan y adalah list yang berisi [2,3,4]. Pertama elemen 1 akan diambil dari list x lalu membandingkannya dengan semua elemen pada y, dan mengecek apakah nilai x sekarang yaitu 1 lebih besar daripada nilai y yang sedang dievaluasi. Jika iya maka x akan ditambah dengan y lalu dimasukan kedalam list. Setelah elemen 1 sudah dievaluasi dengan semua elemen pada list y, x akan berpindah ke elemen 2, mengevaluasinya dengan semua list y dan seterusnya sampai akhir dari list x.